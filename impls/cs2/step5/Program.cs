﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace uk.osric.mal {

    public class Program {

        private readonly Env repl_env = new(null);
        private readonly Reader reader = new Reader();

        private IMalType EvalAst(Env env, IMalType ast) {
            if (ast is MalSymbol s) {
                return env.Get(s);
            } else if (ast is MalList l) {
                return new MalList(l.Select(m => Eval(env, m)));
            } else if (ast is MalVector v) {
                return new MalVector(v.Select(m => Eval(env, m)));
            } else if (ast is MalHash h) {
                MalHash result = new();
                foreach (var kv in h) {
                    IMalType key = kv.Key;
                    IMalType value = Eval(env, kv.Value);
                    result.Add(key, value);
                }
                return result;
            } else {
                return ast;
            }
        }

        private IMalType Eval(Env env, IMalType ast) {
            while (true) {
                if (ast is MalList l) {
                    if (l.IsEmpty) {
                        return l;
                    }
                    if (l.Head is MalSymbol symb) {
                        MalList rest = l.Tail;
                        switch (symb.Value) {
                            case "def!":
                                return env.Set((MalSymbol)rest.Head, Eval(env, rest.Tail.Head));
                            case "do": {
                                    // This must evaluate right away
                                    rest.SkipLast(1).Select(m => Eval(env, m)).ToList();
                                    ast = rest.Last();
                                    continue;
                                }
                            case "if": {
                                    IMalType testResult = Eval(env, rest.Head);
                                    if (testResult != IMalType.Nil && testResult != IMalType.False) {
                                        ast = rest.Tail.Head;
                                    } else {
                                        ast = rest.Tail.Tail.Head;
                                    }
                                    continue;
                                }
                            case "let*": {
                                    Env inner = new Env(env);
                                    if (rest.Head is MalList bindingList) {
                                        // via https://stackoverflow.com/a/6888263
                                        using (var iterator = bindingList.GetEnumerator()) {
                                            while (iterator.MoveNext()) {
                                                IMalType key = iterator.Current;
                                                IMalType value = iterator.MoveNext() ? iterator.Current : throw new ArgumentException("Bindings must come in pairs");
                                                inner.Set((MalSymbol)key, Eval(inner, value));
                                            }
                                        }
                                    } else if (rest.Head is MalVector bindingVector) {
                                        for (int i = 0; i < bindingVector.Count; i += 2) {
                                            MalSymbol key = (MalSymbol)bindingVector[i];
                                            IMalType value = bindingVector[i + 1];
                                            inner.Set(key, Eval(inner, value));
                                        }
                                    } else {
                                        throw new Exception("Unknown type for 'let*' bindings");
                                    }
                                    env = inner;
                                    ast = rest.Tail.Head;
                                    continue;
                                }
                            case "fn*":
                                return new MalMalFunc(
                                    env,
                                    rest.Tail.Head,
                                    (MalList)rest.Head
                                );
                        }
                    }

                    // Call
                    MalList list = (MalList)EvalAst(env, ast);
                    if (list.Head is MalNativeFunc native) {
                        return native.Apply(list.Tail);
                    } else if (list.Head is MalMalFunc func) {
                        ast = func.Body;
                        env = new Env(func.Env, func.Param, list.Tail);
                    }
                } else {
                    return EvalAst(env, ast);
                }
            }
        }

        private IMalType Read(string input) {
            return reader.ReadStr(input);
        }

        private string Print(IMalType input) {
            return Printer.PrStr(input, true);
        }

        private string Rep(string input) {
            return Print(Eval(repl_env, Read(input)));
        }


        private void LoadStdLib(Env env) {
            // Load standard lib native functions
            foreach ((MalSymbol key, MalFunc value) in Core.NS) {
                env.Set(key, new MalNativeFunc(value));
            }

            // Load standard lib mal functions
            foreach (string def in Core.Mal) {
                Rep(def);
            }
        }

        public void Repl(string[] args) {
            LoadStdLib(repl_env);

            Readline readline = new();

            while (true) {
                try {
                    string? input = readline.WaitForInput("user> ", basic: true);
                    if (input != null) {
                        Console.WriteLine(Rep(input));
                    }
                } catch (Exception ex) {
                    Console.Error.WriteLine($"There was a problem {ex.Message}");
                    Console.Error.WriteLine(Printer.FormatException(ex));
                }
            }
        }

        public static void Main(string[] args) {
            Program mal = new Program();
            mal.Repl(args);
        }
    }
}
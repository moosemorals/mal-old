

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;

namespace uk.osric.mal {
    public static class Core {

        [AttributeUsage(AttributeTargets.Method)]
        internal class CoreFuncAttribute : Attribute {
            public string Name { get; init; }
            public CoreFuncAttribute(string name) => Name = name;
        }

        [CoreFunc("+")]
        public static IMalType Add(MalList l) {
            return l.Cast<MalNumber>().Aggregate((t, m) => new MalNumber(t.Value + m.Value));
        }

        [CoreFunc("*")]
        public static IMalType Subtract(MalList l) {
            return l.Cast<MalNumber>().Aggregate((t, m) => new MalNumber(t.Value * m.Value));
        }

        [CoreFunc("-")]
        public static IMalType Minus(MalList l) {
            return l.Cast<MalNumber>().Aggregate((t, m) => new MalNumber(t.Value - m.Value));
        }

        [CoreFunc("/")]
        public static IMalType Divide(MalList l) {
            return l.Cast<MalNumber>().Aggregate((t, m) => new MalNumber(t.Value / m.Value));
        }

        private static IMalType BoolWrapper(bool b) => b ? IMalType.True : IMalType.False;

        [CoreFunc("<")]
        public static IMalType LessThan(MalList l) => BoolWrapper(((MalNumber)l.First).Value < ((MalNumber)l.Rest.First).Value);

        [CoreFunc("<=")]
        public static IMalType LessThanEqual(MalList l) => BoolWrapper(((MalNumber)l.First).Value <= ((MalNumber)l.Rest.First).Value);

        [CoreFunc(">")]
        public static IMalType GreaterThan(MalList l) => BoolWrapper(((MalNumber)l.First).Value > ((MalNumber)l.Rest.First).Value);

        [CoreFunc(">=")]
        public static IMalType GreaterThanEqual(MalList l) => BoolWrapper(((MalNumber)l.First).Value >= ((MalNumber)l.Rest.First).Value);

        [CoreFunc("=")]
        public static IMalType Equal(MalList l) => BoolWrapper(l.First.Equals(l.Rest.First));

        [CoreFunc("atom")]
        public static IMalType Atom(MalList l) => new MalAtom(l.First);

        [CoreFunc("atom?")]
        public static IMalType AtomP(MalList l) => BoolWrapper(l.First is MalAtom);

        [CoreFunc("concat")]
        public static IMalType Concat(MalList l) =>
            new MalList(l.SelectMany(m
                => m switch {
                    IMalSeq s => s,
                    IMalType t => new MalList(new IMalType[] { t })
                }
            ));

        [CoreFunc("cons")]
        public static IMalType Cons(MalList l) => l.Rest.First switch {
            MalList list => list.Cons(l.First),
            IMalSeq seq => new MalList(seq).Cons(l.First),
            _ => throw new MalRuntimeException("Not a list type")
        };

        [CoreFunc("count")]
        public static IMalType Count(MalList l) {
            return new MalNumber(l.First is MalNil ? 0 : ((IMalSeq)l.First).Count());
        }

        [CoreFunc("deref")]
        public static IMalType Deref(MalList l) => ((MalAtom)l.First).Value;

        [CoreFunc("empty?")]
        public static IMalType EmptyP(MalList l) => BoolWrapper(((IMalSeq)l.First).Any());

        [CoreFunc("first")]
        public static IMalType First(MalList l) => l.First switch {
            MalNil => IMalType.Nil,
            MalVector { Count: 0 } => IMalType.Nil,
            MalList ll => ll.First,
            MalVector v => new MalList(v).First,
            _ => throw new MalRuntimeException("not a list type")
        };

        [CoreFunc("list")]
        public static IMalType List(MalList l) => new MalList(l);

        [CoreFunc("list?")]
        public static IMalType ListP(MalList l) => BoolWrapper(l.First is MalList);

        [CoreFunc("nth")]
        public static IMalType Nth(MalList l) {
            if (l.First is IMalSeq s) {
                int index = (int)(((MalNumber)l.Rest.First).Value);
                return s.ElementAt(index);
            }
            throw new MalRuntimeException("Not a list type");
        }

        [CoreFunc("pr-str")]
        public static IMalType PrStr(MalList l) {
            return new MalString(string.Join(" ", l.Select(m => Printer.PrStr(m, true))));
        }

        [CoreFunc("read-string")]
        public static IMalType ReadString(MalList l) {
            return new Reader().ReadStr(((MalString)l.First).Value);
        }

        [CoreFunc("reset!")]
        public static IMalType Reset(MalList l) => ((MalAtom)l.First).Set(l.Rest.First);

        [CoreFunc("rest")]
        public static IMalType Rest(MalList l) => l.First switch {
            MalList m => m.IsEmpty ? MalList.Empty() : m.Rest,
            MalVector v => v.Count == 0 ? MalList.Empty() : new MalList(v.Skip(1)),
            MalNil => MalList.Empty(),
            _ => throw new Exception("Not a list type")
        };

        [CoreFunc("slurp")]
        public static IMalType Slurp(MalList l)
            => new MalString(File.ReadAllText(((MalString)l.First).Value, Encoding.UTF8));

        [CoreFunc("str")]
        public static IMalType Str(MalList l)
            => new MalString(string.Join("", l.Select(m => Printer.PrStr(m, false))));

        [CoreFunc("throw")]
        public static IMalType Throw(MalList l) => throw new MalUserException(l.First);

        [CoreFunc("vec")]
        public static IMalType Vec(MalList l) => l.First switch {
            MalVector v => v,
            MalList list => new MalVector(list),
            IMalType m => new MalVector() { m }
        };

        [CoreFunc("prn")]
        public static IMalType Prn(MalList l) {
            Console.Out.WriteLine(string.Join(" ", l.Select(m => Printer.PrStr(m, true))));
            return IMalType.Nil;
        }

        [CoreFunc("println")]
        public static IMalType PrintLn(MalList l) {
            Console.Out.WriteLine(string.Join(" ", l.Select(m => Printer.PrStr(m, false))));
            return IMalType.Nil;
        }
        public static List<string> Mal = new() {
            "(def! not (fn* (a) (if a false true)))",
            "(def! load-file (fn* (path) (eval (read-string (str \"(do \" (slurp path) \"\nnil)\")))))",
            "(defmacro! cond (fn* (& xs) (if (> (count xs) 0) (list 'if (first xs) (if (> (count xs) 1) (nth xs 1) (throw \"odd number of forms to cond\")) (cons 'cond (rest (rest xs)))))))",

        };
    }
}
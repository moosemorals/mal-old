

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;

namespace uk.osric.mal {


    public static class Core {

        private static bool AreEqual(IMalType left, IMalType right) => left.Equals(right);

        public static Dictionary<MalSymbol, MalFunc> NS = new() {
            { new MalSymbol("+"),       l =>  l.Cast<MalNumber>().Aggregate((t, m) => new MalNumber( t.Value + m.Value )) },
            { new MalSymbol("*"),       l =>  l.Cast<MalNumber>().Aggregate((t, m) => new MalNumber( t.Value * m.Value )) },
            { new MalSymbol("-"),       l =>  l.Cast<MalNumber>().Aggregate((t, m) => new MalNumber( t.Value - m.Value )) },
            { new MalSymbol("/"),       l =>  l.Cast<MalNumber>().Aggregate((t, m) => new MalNumber( t.Value / m.Value )) },
            { new MalSymbol("<"),       l => ((MalNumber)l.First).Value < ((MalNumber)l.Rest.First).Value ? IMalType.True : IMalType.False },
            { new MalSymbol("<="),      l => ((MalNumber)l.First).Value <= ((MalNumber)l.Rest.First).Value ? IMalType.True : IMalType.False },
            { new MalSymbol("="),       l => AreEqual(l.First, l.Rest.First) ? IMalType.True : IMalType.False },
            { new MalSymbol(">"),       l => ((MalNumber)l.First).Value > ((MalNumber)l.Rest.First).Value ? IMalType.True : IMalType.False },
            { new MalSymbol(">="),      l => ((MalNumber)l.First).Value >= ((MalNumber)l.Rest.First).Value ? IMalType.True : IMalType.False },
            { new MalSymbol("atom"),    l => new MalAtom(l.First) },
            { new MalSymbol("atom?"),   l => l.First is MalAtom ? IMalType.True : IMalType.False },
            { new MalSymbol("concat"),  l => new MalList(l.SelectMany(m => m switch { IMalSeq s => s, IMalType t => new MalList( new IMalType[] { t } ) })) },
            { new MalSymbol("cons"),    l => l.Rest.First switch {
                MalList list => list.Cons(l.First),
                IMalSeq seq => new MalList(seq).Cons(l.First),
                _ => throw new MalRuntimeException("Not a list type")
            } },
            { new MalSymbol("count"),   l => new MalNumber( l.First is MalNil ? 0 : ((IMalSeq)l.First).Count()) },
            { new MalSymbol("deref"),   l => ((MalAtom)l.First).Value },
            { new MalSymbol("empty?"),  l => ((IMalSeq)l.First).Any() ? IMalType.False : IMalType.True },
            { new MalSymbol("first"),   l => l.First switch {
                MalNil => IMalType.Nil,
                MalVector { Count: 0 } => IMalType.Nil,
                MalList ll => ll.First,
                MalVector v => new MalList(v).First,
                _ => throw new MalRuntimeException("not a list type")
            } },
            { new MalSymbol("list"),    l => new MalList(l) },
            { new MalSymbol("list?"),   l => l.First is MalList ? IMalType.True : IMalType.False },
            { new MalSymbol("nth"),     l => l.First is IMalSeq s ? s.ElementAt((int)(((MalNumber)l.Rest.First).Value)) : throw new MalRuntimeException("Not a list type") },
            { new MalSymbol("pr-str"),  l => new MalString(string.Join(" ", l.Select(m => Printer.PrStr(m, true)))) },
            { new MalSymbol("read-string"), l => new Reader().ReadStr(((MalString)l.First).Value) },
            { new MalSymbol("reset!"),  l => ((MalAtom)l.First).Set(l.Rest.First) },
            { new MalSymbol("rest"),    l => l.First switch {
                MalList m => m.IsEmpty ? MalList.Empty() : m.Rest,
                MalVector v => v.Count == 0 ? MalList.Empty() : new MalList(v.Skip(1)),
                MalNil => MalList.Empty(),
                _ => throw new Exception("Not a list type")
            } },
            { new MalSymbol("slurp"),   l => new MalString(File.ReadAllText(((MalString)l.First).Value, Encoding.UTF8)) },
            { new MalSymbol("str"),     l => new MalString(string.Join("", l.Select(m => Printer.PrStr(m, false)))) },
            { new MalSymbol("vec"),     l => l.First switch { MalVector v => v, MalList list => new MalVector(list), IMalType m => new MalVector() { m } } },
            { new MalSymbol("prn"), l => {
                Console.Out.WriteLine(string.Join(" ", l.Select(m => Printer.PrStr(m, true))));
                return IMalType.Nil;
            }},
            { new MalSymbol("println"), l => {
                Console.Out.WriteLine(string.Join(" ", l.Select(m => Printer.PrStr(m, false))));
                return IMalType.Nil;
            }},
       };

        public static List<string> Mal = new() {
            "(def! not (fn* (a) (if a false true)))",
            "(def! load-file (fn* (path) (eval (read-string (str \"(do \" (slurp path) \"\nnil)\")))))",
            "(defmacro! cond (fn* (& xs) (if (> (count xs) 0) (list 'if (first xs) (if (> (count xs) 1) (nth xs 1) (throw \"odd number of forms to cond\")) (cons 'cond (rest (rest xs)))))))",

        };
    }

}